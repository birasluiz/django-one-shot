from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "list_detail": list_detail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, pk):
    detail = get_object_or_404(TodoList, pk=pk)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=detail)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoForm(instance=detail)
    context = {
        "form": form,
        "todo_object": detail,
    }

    return render(request, "todos/edit.html", context)
